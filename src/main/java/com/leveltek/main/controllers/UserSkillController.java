package com.leveltek.main.controllers;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.leveltek.main.entities.Developper;
import com.leveltek.main.entities.Skill;
import com.leveltek.main.entities.User;
import com.leveltek.main.entities.UserSkill;
import com.leveltek.main.repositories.SkillRepository;
import com.leveltek.main.repositories.UserRepository;
import com.leveltek.main.repositories.UserSkillRepository;

@CrossOrigin
@RestController
public class UserSkillController {
	
	@Autowired
	private UserSkillRepository userSkillRepository;
	@Autowired
	private UserRepository userRepository;
	@SuppressWarnings("unused")
	@Autowired
	private SkillRepository skillRepository;
	
	@GetMapping("/user_skills")
    public List<UserSkill> getUserSkills() {
        return (List<UserSkill>) userSkillRepository.findAll();
    }
	
	@PostMapping("/user_skills")
	public void saveUserSkills(@RequestBody List<UserSkill> userSkills) {
		System.out.println(userSkills);
		userSkillRepository.saveAll(userSkills);
	}
	
	@PostMapping("/user_skills/delete")
	public void deleteUserSkills(@RequestBody List<UserSkill> userSkills){
		userSkillRepository.deleteAll(userSkills);
	}
	
	@PostMapping("/user_skills/delete_one")
	public void deleteUserSkill(@RequestBody UserSkill userSkill){
		userSkillRepository.delete(userSkill);
	}
	
}
