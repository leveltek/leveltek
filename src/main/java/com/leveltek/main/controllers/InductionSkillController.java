package com.leveltek.main.controllers;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.leveltek.main.entities.Developper;
import com.leveltek.main.entities.InductionSkill;
import com.leveltek.main.entities.Skill;
import com.leveltek.main.entities.User;
import com.leveltek.main.entities.UserSkill;
import com.leveltek.main.repositories.SkillRepository;
import com.leveltek.main.repositories.UserRepository;

import java.util.ArrayList;

@CrossOrigin
@RestController
public class InductionSkillController {
	@Autowired
	private SkillRepository skillRepository;
	@Autowired
	private UserRepository userRepository;
	
	@GetMapping("/search_profile")
	public List<User> searchProfile(@RequestBody List<InductionSkill> pointsSkill){
		List<User> allUsers =  userRepository.findAll();
		Map<Double, Developper> mapUserScoreDif= new TreeMap();
		ArrayList<User> users = new ArrayList<User>(mapUserScoreDif.values());

		
		for(InductionSkill inducSkill: pointsSkill) {
			for(User user: allUsers) {
				Developper dev = (Developper) user;
				for(UserSkill userSkill: dev.getUserSkills()) {
					if(inducSkill.getId() == userSkill.getId().getSkill().getId()) {
						double tmp = inducSkill.getLevel() - userSkill.getScore();
						tmp = Math.pow(tmp, 2.0);
						tmp = Math.sqrt(tmp);
						mapUserScoreDif.put(tmp, dev);
					}
				}
			}
		}
		
		return users;
		
	}
	
	
}
