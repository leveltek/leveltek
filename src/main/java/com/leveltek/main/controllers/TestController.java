package com.leveltek.main.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.leveltek.main.entities.Developper;
import com.leveltek.main.entities.Skill;
import com.leveltek.main.entities.User;
import com.leveltek.main.entities.UserSkill;
import com.leveltek.main.repositories.AnswerRepository;
import com.leveltek.main.repositories.CompanyRepository;
import com.leveltek.main.repositories.DevelopperRepository;
import com.leveltek.main.repositories.QcmRepository;
import com.leveltek.main.repositories.QuestionRepository;
import com.leveltek.main.repositories.ResultQcmRepository;
import com.leveltek.main.repositories.SkillRepository;
import com.leveltek.main.repositories.UserRepository;
import com.leveltek.main.repositories.UserSkillRepository;

@CrossOrigin
@RestController
public class TestController {
	
	//Repositories
	@Autowired
	private QcmRepository qcmRepository;
	@Autowired
	private AnswerRepository answerRepository;
	@Autowired
	private QuestionRepository questionRepository;
	@Autowired
	private ResultQcmRepository resultQcmRepository;
	@Autowired
	private SkillRepository skillRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private DevelopperRepository devRepository;
	@Autowired
	private CompanyRepository companyRepository;
	@Autowired
	private UserSkillRepository skillUserRepository;
	
	@GetMapping("/test1")
	public void addSomeUserSkill(){
		User user = userRepository.findById(1).orElse(null);
		Developper dev = (Developper) user;
		List<Skill> skills = skillRepository.findAll();
		for(Skill skill: skills) {
			int tmp = (int) (Math.random() * 9 + 1);
			skillUserRepository.save(new UserSkill(dev, skill, tmp));
		}
	}	
	
	@GetMapping("/test2")
	public List<UserSkill> testGetSkillsForOneDev(){
		User user = userRepository.findById(13).orElse(null);
		Developper dev = (Developper) user;
		return dev.getUserSkills();
	}
			
	@GetMapping("/test3")
	public Optional<List<Developper>> testSkillsSearchRh(){
		return null;
		/*List<Integer> skillsId = new ArrayList();
		
		// CHERCHER LES DEVS EN JAVA ET JS
		skillsId.add(2);
		skillsId.add(3);
		// CHERCHER LES DEVS EN C# ET VB.NET
		//skillsId.add(31);
		//skillsId.add(32);
		
		return devRepository.findDistinct_ByUserSkills_Id_Skill_Id_In(skillsId);*/
	}
	
	@GetMapping("/test4")
	public void insertRhWithProfiles() {
		
	}
		
}


