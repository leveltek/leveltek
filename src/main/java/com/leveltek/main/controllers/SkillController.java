package com.leveltek.main.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.leveltek.main.entities.Skill;
import com.leveltek.main.entities.User;
import com.leveltek.main.entities.UserSkill;
import com.leveltek.main.repositories.SkillRepository;

@CrossOrigin
@RestController
public class SkillController {
	@Autowired
	private SkillRepository skillRepository;
	
	@PostMapping("/skills")
    public Skill addSkill(@RequestBody Skill skill) {
    	Skill newSkill = this.skillRepository.save(skill);
    	return newSkill;
    }
	
	/**
	 * requête HTTP : GET /skills 
	 * 
	 * ou GET /skills?search=STRING
	 * 
	 * @return Les compétences par filtre search
	 */
	@GetMapping("/skills")
	public List<Skill> findByText(String search) {
		if(search ==null) {
			return findAll();
		}
		return ((List<Skill>) skillRepository.findByText(search));
	}
	
	@GetMapping("/skills_all")
	public List<Skill> findAll() {
		return (List<Skill>) skillRepository.findAll();
	}
	

	
	
}
