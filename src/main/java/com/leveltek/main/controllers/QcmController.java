package com.leveltek.main.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.leveltek.main.entities.Qcm;
import com.leveltek.main.entities.Question;
import com.leveltek.main.repositories.QcmRepository;
import com.leveltek.main.repositories.QuestionRepository;

@CrossOrigin
@RestController
public class QcmController {
	
	@Autowired
	private QcmRepository qcmRepository;
	@Autowired
	private QuestionRepository questionRepository;
	
	@GetMapping("/qcms")
    public List<Qcm> getQcms() {
        return (List<Qcm>) qcmRepository.findAll();
    }
	
	@GetMapping("/qcms/{id}")
	 Qcm oneQcm(@PathVariable int id) {
		//System.out.println(qcmRepository.findById(id).getQuestionAnswers().get(1));
	   return qcmRepository.findById(id);
	 }
	
	@PostMapping("/qcms")
    Qcm addQcm(@RequestBody Qcm qcm) {
    	return qcmRepository.save(qcm);
    }
	@PostMapping("/questions")
    Question addQuestion(@RequestBody Question question) {
    	return questionRepository.save(question);
    }
}
