package com.leveltek.main.controllers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.leveltek.main.entities.Company;
import com.leveltek.main.entities.Developper;
import com.leveltek.main.entities.InductionSkill;
import com.leveltek.main.entities.Qcm;
import com.leveltek.main.entities.ResultQcm;
import com.leveltek.main.entities.ResultQcmKey;
import com.leveltek.main.entities.Skill;
import com.leveltek.main.entities.User;
import com.leveltek.main.entities.UserSkill;
import com.leveltek.main.entities.UserSkillKey;
import com.leveltek.main.payload.UserIdentityAvailability;
import com.leveltek.main.payload.UserSummary;
import com.leveltek.main.repositories.DevelopperRepository;
import com.leveltek.main.repositories.ResultQcmRepository;
import com.leveltek.main.repositories.SkillRepository;
import com.leveltek.main.repositories.UserRepository;
import com.leveltek.main.security.CurrentUser;
import com.leveltek.main.security.UserPrincipal;

@CrossOrigin
@RestController
public class UserController {
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private ResultQcmRepository resultQcmRepository;
	@Autowired
	private DevelopperRepository devRepository;
	@Autowired
	private SkillRepository skillRepository;
	
	@GetMapping("/users")
    public List<User> getUsers() {
        return (List<User>) userRepository.findAll();
    }
	
	@GetMapping("/devs")
    public List<Developper> getDevs() {
        return (List<Developper>) devRepository.findAll();
    }
	
	@PostMapping("/companies")
    void addCompany(@RequestBody Company client) {
		System.out.println(client);
    	userRepository.save(client);
    }
	
	@PostMapping("/devs")
    Developper addDev(@RequestBody Developper client) {
		System.out.println(client);
		System.out.println(client.getId());
		for(UserSkill userSkill: client.getUserSkills()) {
			userSkill.getId().setDevelopper(client);
		}
    	return userRepository.save(client);
    }
	
	@GetMapping("/users/{id}")
    Optional<User> one(@PathVariable int id) {
      return userRepository.findById(id);
    }
	
	@PostMapping("/login")
    public int login(@RequestBody User client) {
    	User cli =null;
    	cli = this.userRepository.findByLoginAndPassword(client.getLogin(), client.getPassword()); 
    	
        if(cli!=null) {
        	return cli.getId();
        }
    	
        return 0;
    }
	
	@PostMapping("/users/result_qcm/{id}/{score}")
	public void addResultQcm(@RequestBody Qcm qcm, @PathVariable int id, @PathVariable int score){
		User user = userRepository.findById(id).orElse(null);
		Developper dev = (Developper) user;
		ResultQcm resultQcm = new ResultQcm(dev, qcm, score);
		System.out.println(resultQcm);
		
		
		resultQcm = resultQcmRepository.save(resultQcm);
		System.out.println(resultQcm);
		//ERREUR ICI 
		//dev.addScore(resultQcm);
		//resultQcm.getId().setDev(dev);
		//resultQcmRepository.save(resultQcm);
	}
	
	
	// FIX FIND BY ID OPTIONAL
	@PostMapping("/users/{id}/skills")
	public void addSkills(@RequestBody List<UserSkill> skills, @PathVariable int id){
		User user = userRepository.findById(id).orElse(null);
		Developper dev = (Developper) user;
		List<UserSkill> tmpSkills = dev.getUserSkills();
		tmpSkills.addAll(skills);
		dev.setUserSkills(tmpSkills);
		
		userRepository.save(dev);
	}
	
	@PostMapping("/rh_search")
	public List<Developper> usersRhSearch(@RequestBody List<InductionSkill> inducSkills) {
		List<Integer> ids = new ArrayList<Integer>();
		List<UserSkill> userSkills = new ArrayList<UserSkill>();
		for(InductionSkill inducSkill: inducSkills) {
			ids.add(inducSkill.getId());
			UserSkill userSkill = new UserSkill();
			UserSkillKey userSkillKey = new UserSkillKey();
			userSkillKey.setSkill(skillRepository.findById(inducSkill.getId()));
			userSkill.setId(userSkillKey);
			userSkills.add(userSkill);
		}
		
		List<Developper> devs = devRepository.findDistinct_ByUserSkills_Id_Skill_Id_In(ids);
		List<Developper> devs2 = new ArrayList<Developper>();
		
		
		
		for(Developper dev: devs) {
			if(dev.getUserSkills().containsAll(userSkills)) {
				devs2.add(dev);
			}
		}
		
		Collections.sort(devs2);
		
		return devs2;
	}
	
	// Ajouts spring security
	
	@GetMapping("/user/me")
    public UserSummary getCurrentUser(@CurrentUser UserPrincipal currentUser) {
        UserSummary userSummary = new UserSummary(currentUser.getId(), currentUser.getUsername());
        return userSummary;
    }

    @GetMapping("/user/checkUsername")
    public UserIdentityAvailability checkUsernameAvailability(@RequestParam(value="username") String username) {
        Boolean isAvailable = !this.userRepository.existsByLogin(username);
        return new UserIdentityAvailability(isAvailable);
    }

    @GetMapping("/user/checkEmailAviability")
    public UserIdentityAvailability checkEmailAvailability(@RequestParam(value="email") String email) {
        Boolean isAvailable = !this.userRepository.existsByMail(email);
        return new UserIdentityAvailability(isAvailable);
    }
    
}
