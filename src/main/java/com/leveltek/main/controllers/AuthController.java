package com.leveltek.main.controllers;

import java.net.URI;
import java.util.Collections;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.leveltek.main.entities.Company;
import com.leveltek.main.entities.Developper;
import com.leveltek.main.entities.Role;
import com.leveltek.main.entities.RoleName;
import com.leveltek.main.entities.User;
import com.leveltek.main.exceptions.AppException;
import com.leveltek.main.payload.ApiResponse;
import com.leveltek.main.payload.JwtAuthenticationResponse;
import com.leveltek.main.payload.LoginRequest;
import com.leveltek.main.payload.SignUpRequest;
import com.leveltek.main.repositories.RoleRepository;
import com.leveltek.main.repositories.UserRepository;
import com.leveltek.main.security.JwtTokenProvider;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JwtTokenProvider tokenProvider;

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = this.authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsernameOrEmail(),
                        loginRequest.getPassword()
                        )
                );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = this.tokenProvider.generateToken(authentication);
        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt, this.userRepository.findByLoginOrMail(loginRequest.getUsernameOrEmail(),loginRequest.getUsernameOrEmail())));
    }

/*
    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {
    	
        if(this.userRepository.existsByLogin(signUpRequest.getUsername())) {
            return new ResponseEntity<>(new ApiResponse(false, "Username is already taken!"),
                    HttpStatus.BAD_REQUEST);
        }

        if(this.userRepository.existsByMail(signUpRequest.getEmail())) {
            return new ResponseEntity<>(new ApiResponse(false, "Email Address already in use!"),
                    HttpStatus.BAD_REQUEST);
        }
        
        // Creating user's account
        User user = new User();
        if(signUpRequest.getName()==null || signUpRequest.getName().equals("")) {
        	Developper dev = new Developper();
        	dev.setLogin(signUpRequest.getUsername());
        	dev.setMail(signUpRequest.getEmail());
        	dev.setPassword(signUpRequest.getPassword());
        	dev.setCity(signUpRequest.getVille());
        	user = dev;
        }else {
        	Company comp = new Company();
        	comp.setLogin(signUpRequest.getUsername());
        	comp.setMail(signUpRequest.getEmail());
        	comp.setPassword(signUpRequest.getPassword());
        	comp.setCity(signUpRequest.getVille());
        	comp.setName(signUpRequest.getName());
        	user = comp;
        }
        
        

        user.setPassword(this.passwordEncoder.encode(user.getPassword()));

        Role userRole = this.roleRepository.findByName(RoleName.ROLE_USER)
                .orElseThrow(() -> new AppException("User Role not set."));

        user.setRoles(Collections.singleton(userRole));

        User result = this.userRepository.save(user);

        URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/api/users/{username}")
                .buildAndExpand(result.getLogin()).toUri();

        return ResponseEntity.created(location).body(new ApiResponse(true, "User registered successfully"));
    }*/
    
    @PostMapping("/signup")
    public User registerUser(@RequestBody SignUpRequest signUpRequest) {
        
        // Creating user's account
        User user = new User();
        if(signUpRequest.getName()==null || signUpRequest.getName().equals("")) {
        	Developper dev = new Developper();
        	dev.setLogin(signUpRequest.getUsername());
        	dev.setMail(signUpRequest.getEmail());
        	dev.setPassword(signUpRequest.getPassword());
        	dev.setCity(signUpRequest.getVille());
        	dev.setTotalScore(signUpRequest.getTotalScore());
        	dev.setLookingForWork(signUpRequest.isLookingForWork());
        	user = dev;
            
        	Role userRole = this.roleRepository.findByName(RoleName.ROLE_DEV)
                    .orElseThrow(() -> new AppException("devUser Role not set."));
            user.setRoles(Collections.singleton(userRole));
            
        }else {
        	Company comp = new Company();
        	comp.setLogin(signUpRequest.getUsername());
        	comp.setMail(signUpRequest.getEmail());
        	comp.setPassword(signUpRequest.getPassword());
        	comp.setCity(signUpRequest.getVille());
        	comp.setName(signUpRequest.getName());
        	user = comp;
        	
        	Role userRole = this.roleRepository.findByName(RoleName.ROLE_RH)
                    .orElseThrow(() -> new AppException("devUser Role not set."));
            user.setRoles(Collections.singleton(userRole));
        }
        
        user.setPassword(this.passwordEncoder.encode(user.getPassword()));



        User result = this.userRepository.save(user);

        URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/api/users/{username}")
                .buildAndExpand(result.getLogin()).toUri();

        return user;
    }
}