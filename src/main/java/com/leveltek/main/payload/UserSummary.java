package com.leveltek.main.payload;

public class UserSummary {

    private int id;
    private String username;
    private String name;

    public UserSummary(int i, String username) {
        this.id = i;
        this.username = username;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
