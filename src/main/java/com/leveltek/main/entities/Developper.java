package com.leveltek.main.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.MapKey;
import javax.persistence.MapKeyColumn;
import javax.persistence.MapKeyJoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.NaturalIdCache;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
@DiscriminatorValue("dev")
public class Developper extends User implements Comparable<Developper> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String firstName;
	private String lastName;
	private Date dateDeNaissance;
	private int totalScore;

	@Column(columnDefinition = "boolean default false")
    private boolean lookingForWork;
	
	@JsonProperty(access = Access.WRITE_ONLY)
	@OneToMany( mappedBy = "id.dev", cascade = CascadeType.ALL)
	@MapKeyColumn(name = "qcm_id")
	private Map<Integer, ResultQcm> scores;

	//@JsonProperty(access = Access.WRITE_ONLY)
	@OneToMany( mappedBy = "id.developper", cascade = CascadeType.ALL)
	private List<UserSkill> userSkills;
	
	public Developper() {
		scores = new HashMap<Integer, ResultQcm>();
		userSkills = new ArrayList<UserSkill>();
	}

	@Override
	public String toString() {
		return "Developper [firstName=" + firstName + ", lastName=" + lastName + ", dateDeNaissance=" + dateDeNaissance
				+ ", totalScore=" + totalScore + ", lookingForWork=" + lookingForWork + ", scores=" + scores
				+ ", userSkills=" + userSkills + "]";
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getDateDeNaissance() {
		return dateDeNaissance;
	}

	public void setDateDeNaissance(Date dateDeNaissance) {
		this.dateDeNaissance = dateDeNaissance;
	}

	public int getTotalScore() {
		return totalScore;
	}

	public void setTotalScore(int totalScore) {
		this.totalScore = totalScore;
	}

	public Map<Integer, ResultQcm> getScores() {
		return scores;
	}

	public void setScores(Map<Integer, ResultQcm> scores) {
		this.scores = scores;
	}
	
	public void addScore(ResultQcm resultQcm) {
		scores.put(resultQcm.getId().getQcm().getId(), resultQcm);
	}

	public List<UserSkill> getUserSkills() {
		return userSkills;
	}

	public void setUserSkills(List<UserSkill> userSkills) {
		this.userSkills = userSkills;
	}
	
	public boolean isLookingForWork() {
		return lookingForWork;
	}

	public void setLookingForWork(boolean lookingForWork) {
		this.lookingForWork = lookingForWork;
	}

	@Override
	    public int compareTo(Developper anotherDev) {
		 int tmp = 0;
		 int tmp2 = 0;
		 for(UserSkill userSkill: this.userSkills) {
			 tmp+=userSkill.getScore();
		 }
		 for(UserSkill userSkill: anotherDev.userSkills) {
			 tmp2+=userSkill.getScore();
		 }
	        return tmp2 - tmp;
	    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((dateDeNaissance == null) ? 0 : dateDeNaissance.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((scores == null) ? 0 : scores.hashCode());
		result = prime * result + totalScore;
		result = prime * result + ((userSkills == null) ? 0 : userSkills.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Developper other = (Developper) obj;
		if (dateDeNaissance == null) {
			if (other.dateDeNaissance != null)
				return false;
		} else if (!dateDeNaissance.equals(other.dateDeNaissance))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (scores == null) {
			if (other.scores != null)
				return false;
		} else if (!scores.equals(other.scores))
			return false;
		if (totalScore != other.totalScore)
			return false;
		if (userSkills == null) {
			if (other.userSkills != null)
				return false;
		} else if (!userSkills.equals(other.userSkills))
			return false;
		return true;
	}
}
