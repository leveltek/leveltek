package com.leveltek.main.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table
public class Profile {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private String name;
	@OneToMany(cascade = CascadeType.ALL)
	private List<InductionSkill> skillsLevel;
	
	public Profile() {
		skillsLevel = new ArrayList<InductionSkill>();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<InductionSkill> getSkillsLevel() {
		return skillsLevel;
	}

	public void setSkillsLevel(List<InductionSkill> skillsLevel) {
		this.skillsLevel = skillsLevel;
	}

	@Override
	public String toString() {
		return "Profile [id=" + id + ", name=" + name + ", skillsLevel=" + skillsLevel + "]";
	}
}
