package com.leveltek.main.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table
@DiscriminatorValue("company")
public class Company extends User{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String siretNum;
	private String name;
	@OneToMany(cascade = CascadeType.ALL)
	private List<Profile> profiles;
	
	
	public Company() {
		profiles = new ArrayList<Profile>();
	}

	public String getSiretNum() {
		return siretNum;
	}

	public void setSiretNum(String siretNum) {
		this.siretNum = siretNum;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Profile> getProfiles() {
		return profiles;
	}

	public void setProfiles(List<Profile> profiles) {
		this.profiles = profiles;
	}
}
