package com.leveltek.main.entities;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table
public class ResultQcm extends AuditModel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@EmbeddedId
	private ResultQcmKey id;
	private int score;
	
	
	public ResultQcm(Developper dev, Qcm qcm, int score) {
		this.score=score;
		id = new ResultQcmKey(qcm, dev);
	}
	
	public ResultQcm() {
		id = new ResultQcmKey();
	}
	
	public ResultQcmKey getId() {
		return id;
	}

	public void setId(ResultQcmKey id) {
		this.id = id;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	@Override
	public String toString() {
		return "ResultQcm [id=" + id +  "]";
	}
	
}
