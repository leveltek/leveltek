package com.leveltek.main.entities;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Embeddable
public class UserSkillKey implements Serializable {
	@ManyToOne(cascade={CascadeType.PERSIST, CascadeType.REMOVE})
	@MapsId("skill_id")
	private Skill skill;
	@JsonProperty(access = Access.WRITE_ONLY)
	@ManyToOne(cascade={CascadeType.PERSIST, CascadeType.REMOVE})
	@MapsId("developper_id")
	private Developper developper;
	
	public UserSkillKey() {
    	
    }

    public UserSkillKey(Skill skill, Developper developper) {
		super();
		this.skill = skill;
		this.developper = developper;
	}
	

	@Override
	public int hashCode() {
		return Objects.hash( developper, skill );
	}

	@Override
	public boolean equals(Object o) {
		if ( this == o ) {
            return true;
        }
        if ( o == null || getClass() != o.getClass() ) {
            return false;
        }
        UserSkillKey that = (UserSkillKey) o;
        return  Objects.equals( skill.getId(), that.skill.getId() );
	}

	public Skill getSkill() {
		return skill;
	}

	public void setSkill(Skill skill) {
		this.skill = skill;
	}

	public Developper getDevelopper() {
		return developper;
	}

	public void setDevelopper(Developper developper) {
		this.developper = developper;
	}

	@Override
	public String toString() {
		return "SkillScoreKey [skill=" + skill + ", developper=" + developper + "  ]";
	}
}
