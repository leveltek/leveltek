package com.leveltek.main.entities;

public enum RoleName {
	ROLE_DEV,
	ROLE_RH,
	ROLE_ADMIN
}
