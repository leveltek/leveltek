package com.leveltek.main.entities;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table
public class AnswerSkill extends AuditModel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private String libelle;
	@OneToOne(cascade={CascadeType.PERSIST, CascadeType.REMOVE})
	@JoinColumn
	private Developper user;
	
	public AnswerSkill(int id, String libelle, Developper user) {
		super();
		this.id = id;
		this.libelle = libelle;
		this.user = user;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public Developper getUser() {
		return user;
	}
	public void setUser(Developper user) {
		this.user = user;
	}
	@Override
	public String toString() {
		return "AnswerSkill [id=" + id + ", libelle=" + libelle + ", user=" + user + "]";
	}
	
	
	

}
