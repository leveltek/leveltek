package com.leveltek.main.entities;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table
public class UserSkill extends AuditModel implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private UserSkillKey id;
	private int score;
	
	public UserSkill(Developper developper, Skill skill, int score) {
		this.score=score;
		id = new UserSkillKey(skill, developper);
	}
	
	public UserSkill() {
		id = new UserSkillKey();
	}

	public UserSkillKey getId() {
		return id;
	}

	public void setId(UserSkillKey id) {
		this.id = id;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	@Override
	public String toString() {
		return "SkillScore [id=" + id + ", score=" + score + "] ";
	}

	@Override
	public boolean equals(Object obj) {
		UserSkill other = (UserSkill) obj;
		return this.id.getSkill().getId()==other.getId().getSkill().getId();
	}
	
}
