package com.leveltek.main.entities;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;

@Embeddable
public class ResultQcmKey implements Serializable {
	

	//@Column(name = "developper_id")
   // private int developperId;
 
   // @Column(name = "qcm_id")
    //private int qcmId;
   
	/**
	 * 
	 */
	@ManyToOne(cascade={CascadeType.PERSIST, CascadeType.REMOVE})
    //@JoinColumn(name = "qcm_id")
	@MapsId("qcm_id")
	private Qcm qcm;
	@ManyToOne(cascade={CascadeType.PERSIST, CascadeType.REMOVE})
    //@JoinColumn(name = "developper_id")
	@MapsId("dev_id")
	private Developper dev;
    
    public ResultQcmKey() {
    	
    }

    public ResultQcmKey(Qcm qcm, Developper dev) {
		super();
		this.qcm = qcm;
		this.dev = dev;
	}
	

	@Override
	public int hashCode() {
		return Objects.hash( dev, qcm );
	}

	@Override
	public boolean equals(Object o) {
		if ( this == o ) {
            return true;
        }
        if ( o == null || getClass() != o.getClass() ) {
            return false;
        }
        ResultQcmKey that = (ResultQcmKey) o;
        return Objects.equals( dev, that.dev ) &&
                Objects.equals( qcm, that.qcm );
	}

	@Override
	public String toString() {
		return "ResultQcmKey [ \n developper=" + dev + "\n qcm=" + qcm + "]";
	}

	public Qcm getQcm() {
		return qcm;
	}

	public void setQcm(Qcm qcm) {
		this.qcm = qcm;
	}

	public Developper getDev() {
		return dev;
	}

	public void setDev(Developper dev) {
		this.dev = dev;
	}
	

}
