package com.leveltek.main.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.leveltek.main.entities.*;

@Repository
public interface UserSkillRepository extends JpaRepository<UserSkill, Integer> { // Long: Type of Employee ID.
	UserSkill findById(int id);

	@Query("SELECT coalesce(max(q.id), 0) FROM UserSkill q")
	int getMaxId();

	void delete(UserSkill skill);
	
}
