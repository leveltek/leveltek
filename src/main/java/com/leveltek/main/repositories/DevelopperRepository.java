package com.leveltek.main.repositories;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.leveltek.main.entities.*;

@Repository
public interface DevelopperRepository  extends JpaRepository<Developper, Integer> { // Long: Type of Employee ID.

	//User findById(int id);
	Developper findByLoginAndPassword(String login, String password);

	@Query("SELECT coalesce(max(u.id), 0) FROM User u")
	int getMaxId();

	void delete(Developper user);
	Optional<Developper> findByLoginOrMail(String usernameOrEmail, String usernameOrEmail2);
	
	Boolean existsByLogin(String login);
	Boolean existsById(int id);
	List<Developper> findByIdIn(List<Integer> userIds);

	Boolean existsByMail(String email);
	
	//Get dev with all skills given by id in the list idSkillsNeed
	List<Developper> findDistinct_ByUserSkills_Id_Skill_Id_In(List<Integer> idSkillsNeed);
	//Optional<List<Developper>> findDistinct_ByUserSkills_In(List<UserSkill> skillsNeed);
}
