package com.leveltek.main.repositories;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.leveltek.main.entities.*;

@Repository
public interface QuestionRepository  extends JpaRepository<Question, Integer> { // Long: Type of Employee ID.

	Question findById(int id);

	@Query("SELECT coalesce(max(q.id), 0) FROM Question q")
	int getMaxId();

	void delete(Question qAnswer);
	
}
