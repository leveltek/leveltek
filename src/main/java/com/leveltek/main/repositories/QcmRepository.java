package com.leveltek.main.repositories;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.leveltek.main.entities.*;

@Repository
public interface QcmRepository  extends JpaRepository<Qcm, Integer> { // Long: Type of Employee ID.

	Qcm findById(int id);

	@Query("SELECT coalesce(max(q.id), 0) FROM Qcm q")
	int getMaxId();

	void delete(Qcm qcm);
	
}
