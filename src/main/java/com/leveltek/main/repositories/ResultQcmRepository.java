package com.leveltek.main.repositories;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.leveltek.main.entities.*;

@Repository
public interface ResultQcmRepository  extends JpaRepository<ResultQcm, ResultQcmKey> { // Long: Type of Employee ID.
	
	//List<ResultQcm> findByResultQcmKey(ResultQcmKey resultQcmKey);
	//ResultQcm findById(ResultQcmKey id);

	void delete(ResultQcm rQcm);
	
}
