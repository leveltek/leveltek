package com.leveltek.main.repositories;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.leveltek.main.entities.*;

@Repository
public interface CompanyRepository  extends JpaRepository<Company, Integer> { // Long: Type of Employee ID.
	void delete(Company company);
}
