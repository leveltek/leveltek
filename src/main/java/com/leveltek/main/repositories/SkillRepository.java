package com.leveltek.main.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.leveltek.main.entities.*;

@Repository
public interface SkillRepository extends JpaRepository<Skill, Integer> { // Long: Type of Employee ID.

	Skill findById(int id);

	@Query("SELECT coalesce(max(q.id), 0) FROM Skill q")
	int getMaxId();
	
	@Query(value = "SELECT * FROM Skill skill WHERE skill.name LIKE %?1% LIMIT 10", nativeQuery=true)
	List<Skill> findByText(String input);


	void delete(Skill skill);
	
}
