package com.leveltek.main.repositories;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.leveltek.main.entities.*;

@Repository
public interface UserRepository  extends JpaRepository<User, Integer> { // Long: Type of Employee ID.

	//User findById(int id);
	User findByLoginAndPassword(String login, String password);

	@Query("SELECT coalesce(max(u.id), 0) FROM User u")
	int getMaxId();

	void delete(User user);
	Optional<User> findByLoginOrMail(String usernameOrEmail, String usernameOrEmail2);
	
	Boolean existsByLogin(String login);
	Boolean existsById(int id);
	List<User> findByIdIn(List<Integer> userIds);

	Boolean existsByMail(String email);
	
}
