package com.leveltek.main;

import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
//@ComponentScan({"com.leveltek.entities","com.leveltek.main","com.leveltek.controllers","com.leveltek.repositories"})
//@EntityScan("com.leveltek.entities", basePackageClasses = {LevelTekApplication.class, Jsr310JpaConverters.class })
//@EnableJpaRepositories("com.leveltek.repositories")
@EntityScan(basePackageClasses = {LevelTekApplication.class, Jsr310JpaConverters.class })
public class LevelTekApplication {
	
	@PostConstruct
	void init() {
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
	}
	public static void main(String[] args) {
		SpringApplication.run(LevelTekApplication.class, args);
	}
	
}
